package nl.tomjansen._aoc_01;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class aoc_01_main {

    public static void main(String[] args) throws FileNotFoundException {

        final Scanner reader = new Scanner(new File("src/main/java/nl/tomjansen/_aoc_01/pzl-01-input.txt"));
        List<Integer> numbers = new ArrayList<>();
        int total = 0;

        while (reader.hasNextLine()) {
            StringBuilder sb = new StringBuilder(reader.nextLine());

            final String firstDigit = findFirstDigit(sb.toString());
            sb.reverse();
            final String lastDigit = findFirstDigit(sb.toString());

            numbers.add(Integer.parseInt(firstDigit + lastDigit));
        }

        System.out.println(numbers);

        for (Integer number : numbers) {
            total += number;
        }

        System.out.println(total);

    }

    public static String findFirstDigit(final String line) {
        CharacterIterator ci = new StringCharacterIterator(line);

        while (ci.current() != CharacterIterator.DONE) {
            if (Character.isDigit(ci.current())) {
                return String.valueOf(ci.current());
            }

            ci.next();
        }

        return "";
    }

}
